//
//  SettingsView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject
    private var authStatus = SyncAuthStatus.shared

    @UserDefaultOptionalWrapper(.username)
    private var storedUsername: String?

    @State
    private var selectedLink: NavLinkTag? = nil

    func logout() {
        authStatus.authStatus = .loggedOut
    }

    func resetApp() {
        logout()
        storedUsername = nil
        try? appDelegate().sync.dropSynchronizationAndDatabase()
    }

    var body: some View {
        NavigationView {
            List {
                Section {
                    if authStatus.isLoggedIn {
                        Button(action: logout, label: {
                            NavigationRow(text: "Log out", icon: "escape", iconColor: .orange)
                            .foregroundColor(.orange)
                        })
                    } else {
                        NavigationLink(destination: LogInView(isRegistration: false, close: returnToRoot), tag: .login, selection: $selectedLink) {
                            NavigationRow(text: "Log in", icon: "person", iconColor: .green)
                        }

                        NavigationLink(destination: LogInView(isRegistration: true, close: returnToRoot), tag: .registration, selection: $selectedLink) {
                            NavigationRow(text: "Registration", icon: "person.badge.plus", iconColor: .orange)
                        }
                    }
                }
                Section {
                    Button(action: resetApp, label: {
                        NavigationRow(text: "Reset app", icon: "trash", iconColor: .red)
                            .foregroundColor(.red)
                    })
                }
            }.navigationBarTitle("Settings")
                .listStyle(GroupedListStyle())
        }
    }

    func returnToRoot() {
        selectedLink = nil
    }
}

extension SettingsView {
    enum NavLinkTag {
        case login
        case registration
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
