//
//  NavigationRow.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI

struct NavigationRow: View {
    let text: String
    let icon: String
    var iconColor: Color = .secondary

    var body: some View {
        HStack {
            Text(text)
            Spacer()
            Image(systemName: icon)
                .foregroundColor(iconColor)

        }
    }
}

struct NavigationRow_Previews: PreviewProvider {
    static var previews: some View {
        NavigationRow(text: "Log in", icon: "person")
            .foregroundColor(.primary)
            .previewLayout(.fixed(width: 300, height: 50))
    }
}
