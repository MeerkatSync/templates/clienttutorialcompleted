//
//  NoteView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct NoteView: View {
    @Environment(\.presentationMode) var present

    private let folder: ObservableSyncObject<NotesFolder>

    @ObservedObject
    private var note: ObservableSyncObject<Note>

    init(note: ObservableSyncObject<Note>, folder: ObservableSyncObject<NotesFolder>) {
        self.note = note
        self.folder = folder
        isEditing = true
        _title = .init(wrappedValue: note.transform { $0?.title } ?? "")
        _text = .init(wrappedValue: note.transform { $0?.text } ?? "")
    }

    init(folder: ObservableSyncObject<NotesFolder>) {
        self.folder = folder
        isEditing = false
        _title = .init(wrappedValue: "")
        _text = .init(wrappedValue: "")
        note = .init()
    }

    let isEditing: Bool
    @State
    private var objectChanged: Bool = false

    @State
    private var title: String

    @State
    private var text: String

    var body: some View {
        NavigationView {
            VStack {
                TextField("Title", text: $title)
                    .modifier(FormRowModifier())
                TextField("text", text: $text)
                    .modifier(FormRowModifier())
                Button(action: save) {
                    Text("Save")
                }.modifier(FormButtonModifier(color: .green))
                Spacer()
            }.padding()
            .alert(isPresented: $objectChanged) {
                Alert(title: Text("Note is updated!"),
                      message: Text("There is updated version of note. Do you want to reload?"),
                      primaryButton: .destructive(Text("Reload"), action: reload),
                      secondaryButton: .cancel(Text("Save current version"), action: save))
            }.navigationBarTitle(isEditing ? title : "New note")
        }.onReceive(note.objectWillChange, perform: didChange)
    }

    func didChange() {
        note.ifValid { note in
            guard text != note.text || title != note.title else {
                return
            }
            DispatchQueue.main.async {
                self.objectChanged = true
            }
        }
    }

    func reload() {
        note.ifValid { note in
            title = note.title
            text = note.text
        }
    }

    func save() {
        if isEditing {
            update()
        } else {
            saveNewNote()
        }
        close()
    }

    func update() {
        note.ifValid { note in
            let realm = getRealm()
            try! realm.write {
                note.title = title
                note.text = text
            }
        }
    }

    func saveNewNote() {
        // 1
        folder.ifValid { folder in
            let realm = getRealm()
            try! realm.write {
                // 2
                let note = Note(title: title, text: text)
                // 3
                note.group = folder.group
                // 4
                realm.add(note)
                // 5
                folder.notes.append(note)
            }
        }
    }

    func closeAlert() {
        objectChanged = false
    }

    func close() {
        self.present.wrappedValue.dismiss()
    }
}

struct NoteView_Previews: PreviewProvider {
    static var previews: some View {
        NoteView(folder: .init())
    }
}
