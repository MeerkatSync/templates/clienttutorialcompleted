//
//  AppDelegate.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import UIKit
import MeerkatClientSyncController
import MeerkatRealmWrapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let sync: SyncController<MeerkatClientDBWrapper> = {
        // 1
        Realm.Configuration.meerkatRealmConfiguration = RealmDefaultConfig()
        // 2
        let db = MeerkatClientDBWrapper(objects: [Note.self, NotesFolder.self],
                                        // 3
                                        onError: { fatalError("\($0)") })
        let controller = SyncController(db: db,
                                        // 4
                                        clientConfig: Constants.clientConfig,
                                        // 5
                                        syncConfig: SyncAuthStatus.shared.syncConfig)
        return controller
    }()

    private var pullTimer: Timer? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // 1
        pullTimer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { [weak self] timer in
            guard self != nil else {
                // 2
                timer.invalidate()
                return
            }
            // 3
            SyncPullRequest.send()
        }
        // 4
        DispatchQueue.global().async {
            SyncPullRequest.send()
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

func appDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}
