//
//  ShareView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 30/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper
import MeerkatClientSyncController

struct ShareView: View {
    @UserDefaultOptionalWrapper(.username)
    private var user: String?

    @ObservedObject
    var group: ObservableSyncObject<SyncGroup>

    @ObservedObject
    private var client = MeerkatClientAPI(clientConfig: Constants.clientConfig)

    @ObservedObject
    private var authStatus = SyncAuthStatus.shared

    @State
    private var username = ""

    @State
    private var users = [PublicGroupUser]()

    @State
    private var task: Cancellable? = nil

    @State
    private var alertType: AlertType?

    private var isOwner: Bool {
        group.transform { $0?.role == .owner }
    }

    var body: some View {
        NavigationView {
            VStack {
                if isOwner {
                    VStack {
                        HStack {
                            Text("Add a user:")
                            Spacer()
                        }
                        TextField("Username", text: $username)
                            .modifier(FormRowModifier())
                        Button(action: add) {
                            Text("Add")
                        }.modifier(FormButtonModifier(color: .green))
                    }.padding()
                }
                HStack {
                    Text("Users:")
                    Spacer()
                }.padding(.leading)
                    .padding(.bottom, -12)
                LoadingView(isShowing: $client.isLoading, cancel: cancelTask) {
                    List {
                        // 1
                        ForEach(self.users, id: \.username) { user in
                            // 2
                            UserShareDetail(username: user.username,
                                            selectedRole: user.role,
                                            roleUpdated: self.isOwner ? self.add : nil)
                        }.onDelete(perform: self.isOwner ? self.remove : nil)
                    }
                }
            }.alert(item: $alertType) {
                $0.alert
            }
        }.onAppear(perform: reloadUsers)
    }

    func reloadUsers() {
        guard let auth = authStatus.syncConfig else {
            alertType = .notLoggedIn
            return
        }
        // 1
        group.ifValid { group in
            task = client
                // 2
                .getUsers(groupId: group.id, token: auth.authToken, userType: PublicGroupUser.self)
                // 3
                .sink(receiveCompletion: handleCompletion) { users in
                    // 4
                    self.users = users
            }
        }
    }

    func add() {
        add(userId: username, role: .readAndWrite)
        username = ""
    }

    func remove(indexes: IndexSet) {
        // 1
        guard let usersUsername = user else {
            alertType = .notLoggedIn
            return
        }
        // 2
        let usersIds = indexes.map { users[$0].username }
        // 3
        if usersIds.contains(usersUsername) {
            // 4
            alertType = .removeSelf({ self.remove(usersIds: usersIds) })
        } else {
            // 5
            remove(usersIds: usersIds)
        }
    }

    func remove(usersIds: [String]) {
        // 1
        guard let auth = getTokenWithGroupId() else {
            alertType = .notLoggedIn
            return
        }
        // 2
        let syncIds = usersIds.map { getSyncId(for: $0) }
        // 3
        let unsubs = syncIds.map { syncIdTask in
            syncIdTask.flatMap { syncId in
                self.client.unsubscribe(syncId: syncId, from: auth.groupId, token: auth.token)
            }
        }
        // 4
        let merged = mergePublishers(unsubs)
        // 5
        task = taskWithReload(merged)
    }

    func add(userId: String, role: Role) {
        // 1
        guard let auth = getTokenWithGroupId() else {
            alertType = .notLoggedIn
            return
        }
        // 2
        let sub = getSyncId(for: userId).flatMap { syncId in
            // 3
            self.client.subscribe(syncId: syncId, to: auth.groupId, as: role, token: auth.token)
        }
        // 4
        task = taskWithReload(sub)
    }

    func getSyncId(for userId: String) -> AnyPublisher<String, Error> {
        // 1
        guard let auth = authStatus.syncConfig, let user = user else {
            alertType = .notLoggedIn
            // 2
            return Empty().eraseToAnyPublisher()
        }
        // 3
        return client.syncIdFor(userId: userId, interestedUserId: user, token: auth.authToken)
    }
}

extension ShareView {
    func mergePublishers<T: Publisher>(_ publishers: [T]) -> AnyPublisher<T.Output, T.Failure> {
        publishers.reduce(Empty().eraseToAnyPublisher()) { acc, elem in
            acc.merge(with: elem).eraseToAnyPublisher()
        }
    }

    func taskWithReload<T: Publisher>(_ task: T) -> Cancellable where T.Output == Void, T.Failure == Error {
        task
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handleCompletion(completion:)) {
                self.reloadUsers()
        }
    }

    func handleCompletion(completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            return
        case .failure(let err as BadRequest):
            alertType = .requestError(err)
        case .failure(let err):
            alertType = .other(err)
        }
    }
}

extension ShareView {
    func getTokenWithGroupId() -> (token: String, groupId: String)? {
        guard let auth = authStatus.syncConfig else {
            return nil
        }

        guard let groupId = group.transform(defaultValue: nil, { $0.id }) else {
            return nil
        }

        return (auth.authToken, groupId)
    }

    func cancelTask() {
        task?.cancel()
        task = nil
    }
}

extension ShareView {
    enum AlertType: Identifiable {
        var id: ObjectIdentifier { .init(Self.self) }
        case notLoggedIn
        case removeSelf(() -> Void)
        case requestError(BadRequest)
        case other(Error)

        var alert: Alert {
            let title: String
            let message: Text?
            switch self {
            case .removeSelf(let cb):
                return Alert(title: Text("Do you really want to leave this group?"), primaryButton: .cancel(), secondaryButton: .destructive(Text("Leave group"), action: cb))
            case .notLoggedIn:
                title = "User is not logged in."
                message = nil
            case .requestError(let err):
                title = "Network error: \(err.code)"
                message = Text(err.body)
            case .other(let err):
                title = "Unown error"
                message = Text(err.localizedDescription)
            }
            return Alert(title: Text(title), message: message)
        }
    }
}

struct ShareView_Previews: PreviewProvider {
    static var previews: some View {
        ShareView(group: .init())
    }
}
