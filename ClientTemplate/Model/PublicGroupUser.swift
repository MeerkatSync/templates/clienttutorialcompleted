//
//  PublicGroupUser.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 05/05/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatCore

struct PublicGroupUser: Codable {
    let username: String
    let role: Role
}
