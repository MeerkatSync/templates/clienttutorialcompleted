//
//  Note.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 05/05/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatRealmWrapper

final class Note: SyncObject, NoteScheme {
    @objc dynamic var id = UUID().uuidString

    @objc dynamic var title = ""

    @objc dynamic var text = ""

    @objc dynamic var created = Date()

    @objc dynamic var isDeleted = false

    @objc dynamic var group: SyncGroup? = nil

    init(title: String, text: String) {
        self.title = title
        self.text = text
    }

    required init() { }
}
