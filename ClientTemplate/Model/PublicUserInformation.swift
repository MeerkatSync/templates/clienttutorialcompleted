//
//  PublicUserInformation.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 05/05/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

struct PublicUserInformation: Codable {
    let username: String
    let password: String
}
