//
//  NotesFolder.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 05/05/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatRealmWrapper

final class NotesFolder: SyncObject, NotesFolderScheme {
    typealias Notes = List<Note>

    @objc dynamic var id = UUID().uuidString

    @objc dynamic var title = ""

    let notes = Notes()

    @objc dynamic var isDeleted = false

    @objc dynamic var group: SyncGroup? = nil

    init(title: String) {
        self.title = title
    }

    required init() { }
}
