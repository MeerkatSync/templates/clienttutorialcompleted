//
//  FoldersView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct FoldersView: View {

    @State
    private var activeFolder: String? = nil

    @ObservedObject
    private var folders = NotesFolder.observableObjects

    private var sortedFolders: [SyncObjectWrapper<NotesFolder>] {
        folders.elements.sorted { $0.title < $1.title }
    }

    @State
    private var newFolder = false

    private func folderLink(for folder: SyncObjectWrapper<NotesFolder>) -> some View {
        // 1
        let observableFolder = folder.observableObject().onDelete {
            self.activeFolder = nil
        }
        // 2
        let destination = FolderDetailView(folder: observableFolder)
        let view = NavigationLink(
            destination: destination,
            // 3
            tag: folder.id,
            // 4
            selection: self.$activeFolder) {
                // 5
                HStack {
                    Text(folder.title)
                    Spacer()
                    Text("(\(folder.notes.count))")
                        .foregroundColor(.secondary)
                }
        }
        return view
    }

    var body: some View {
        NavigationView {
            List {
                ForEach(sortedFolders) { folder in
                    self.folderLink(for: folder)
                }
                .onDelete(perform: deleteFolders)
            }.navigationBarTitle("Folders")
            .navigationBarItems(trailing: Button(action: {
                self.newFolder = true
            }) {
                Image(systemName: "folder.badge.plus")
            })
                .sheet(isPresented: $newFolder) {
                    NewFolderView()
            }
        }
    }

    func deleteFolders(indexes: IndexSet) {
        let realm = getRealm()
        try! realm.write {
            // 1
            let foldersToDelete = indexes.map { sortedFolders[$0] }
            foldersToDelete.forEach { folder in
                // 2
                folder.ifValid { folder in
                    // 3
                    folder.notes.forEach {
                        $0.isDeleted = true
                    }
                    // 4
                    folder.isDeleted = true
                    // 5
                    folder.group?.isDeleted = true
                }
            }
        }
    }
}

struct FoldersView_Previews: PreviewProvider {
    static var previews: some View {
        FoldersView()
    }
}
