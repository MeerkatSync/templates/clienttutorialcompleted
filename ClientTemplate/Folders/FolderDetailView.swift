//
//  FolderDetailView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct FolderDetailView: View {

    @State
    private var isEditing: SheetType? = nil

    // 1
    @ObservedObject
    private var folder: ObservableSyncObject<NotesFolder>

    // 2
    init(folder: ObservableSyncObject<NotesFolder>) {
        self.folder = folder
    }

    // 3
    #if DEBUG
    init() {
        folder = ObservableSyncObject()
    }
    #endif

    private var notes: [SyncObjectWrapper<Note>] {
        // 1
        folder.transform(defaultValue: []) { folder in
            // 2
            folder.notes.sorted(byKeyPath: \.created, ascending: false).wrappedArray
        }
    }

    var body: some View {
        VStack {
            List {
                ForEach(notes, id: \.id) { note in
                    Button(action: {
                        self.isEditing = .editNote(note)
                    }) {
                        Text(note.title)
                    }
                }.onDelete(perform: deleteNotes)
            }.navigationBarTitle("Folder")
                .navigationBarItems(trailing: HStack {
                    Button(action: {
                        self.isEditing = .share
                    }) {
                        Image(systemName: "person.3")
                    }.padding(.trailing)
                    Button(action: {
                        self.isEditing = .addNote
                    }) {
                        Image(systemName: "plus")
                    }
                })

        }.sheet(item: $isEditing) { sheet in
            sheet.view(folder: self.folder) {
                DispatchQueue.main.async {
                    self.isEditing = nil
                }
            }
        }
    }

    func deleteNotes(indexes: IndexSet) {
        // 1
        let notesToDelete = indexes.map { notes[$0] }
        let realm = getRealm()
        try! realm.write {
            notesToDelete.forEach { note in
                // 2
                note.ifValid { note in
                    note.isDeleted = true
                }
            }
        }
    }
}

extension FolderDetailView {
    enum SheetType: Identifiable {
        var id: ObjectIdentifier { .init(SheetType.self) }

        case addNote
        case share
        case editNote(SyncObjectWrapper<Note>)

        func view(folder: ObservableSyncObject<NotesFolder>, close: @escaping () -> Void) -> some View {
            // 1
            let folderObservableCopy = folder.observableCopy
            // 2
            let folder = folderObservableCopy.onDelete(call: close)
            switch self {
            case .addNote:
                return AnyView(NoteView(folder: folder))
            case .share:
                // 1
                guard let group = folder.transform( { $0?.group?.observableObject() }) else {
                    return AnyView(EmptyView())
                }
                // 2
                return AnyView(ShareView(group: group.onDelete(call: close)))
            case .editNote(let n):
                let note = n.observableObject().onDelete(call: close)
                return AnyView(NoteView(note: note, folder: folder))
            }
        }
    }
}

struct FolderDetail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            FolderDetailView()
        }
    }
}
