//
//  NewFolderView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 25/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct NewFolderView: View {
    @Environment(\.presentationMode) var present

    @State
    private var title: String = ""

    @State
    private var invalidTitle = false

    var body: some View {
        NavigationView {
            VStack {
                TextField("Title", text: $title)
                    .modifier(FormRowModifier())
                Button(action: save) {
                    Text("Save")
                }.modifier(FormButtonModifier(color: .blue))
                Spacer()
            }.padding()
            .navigationBarTitle("New folder")
        }
            .alert(isPresented: $invalidTitle) {
            Alert(title: Text("Title is ivalid"))
        }
    }

    private func save() {
        // 1
        let realm = getRealm()

        // 2
        try! realm.write {
            // 3
            let newGroup = SyncGroup()
            realm.add(newGroup)

            // 4
            let folder = NotesFolder(title: title)
            folder.group = newGroup
            realm.add(folder)
        }

        present.wrappedValue.dismiss()
    }
}

struct NewFolderView_Previews: PreviewProvider {
    static var previews: some View {
        NewFolderView()
    }
}
