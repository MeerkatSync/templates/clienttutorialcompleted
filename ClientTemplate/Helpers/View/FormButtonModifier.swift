//
//  FormButtonModifier.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI

struct FormButtonModifier: ViewModifier {
    let color: Color
    func body(content: Content) -> some View {
        content
            .font(.title)
            .accentColor(.white)
            .padding(.horizontal, 10)
            .padding(.vertical, 3)
            .background(color)
            .cornerRadius(10)
    }
}
